package com.glaines.avanzadotarea04.storage

import androidx.lifecycle.LiveData
import com.glaines.avanzadotarea04.storage.db.DBServiceDTO

interface DataSource {

    fun listServices(): LiveData<List<DBServiceDTO>>
    fun add(service: DBServiceDTO)
    fun update(id: Int, service: DBServiceDTO)
    fun delete(id: Int, service: DBServiceDTO)

}