package com.glaines.avanzadotarea04.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.glaines.avanzadotarea04.di.Injector
import com.glaines.avanzadotarea04.databinding.FragmentAddServiceBinding
import com.glaines.avanzadotarea04.model.Service
import kotlinx.coroutines.launch
import java.util.*

class AddServiceFragment : Fragment() {

    private val repository by lazy {
        Injector.provideServiceRepository()
    }

    private var _binding: FragmentAddServiceBinding? = null
    private val binding get() = _binding!!

    private var name: String = ""
    private var desc: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddServiceBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnAddService.setOnClickListener {
            if (validForm()) {
                addService()
            }
        }
    }

    private fun addService() {
        val service = Service(0,name, desc, Date())
        lifecycleScope.launch {
            repository.addService(service)
            findNavController().popBackStack()
        }

    }

    private fun validForm(): Boolean {
        clearForm()
        name = binding.edtName.text.toString().trim()
        desc = binding.edtDesc.text.toString().trim()

        if (name.isEmpty()) {
            binding.edtName.error = "Campo nombre inválido"
            return false
        }

        if (desc.isEmpty()) {
            binding.edtDesc.error = "Campo descripción inválido"
            return false
        }
        return true
    }

    private fun clearForm() {
        binding.edtName.error = null
        binding.edtDesc.error = null
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}