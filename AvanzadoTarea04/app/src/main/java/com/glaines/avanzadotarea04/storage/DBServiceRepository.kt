package com.glaines.avanzadotarea04.storage

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.glaines.avanzadotarea04.AppExecutors
import com.glaines.avanzadotarea04.model.Service
import com.glaines.avanzadotarea04.storage.db.DBServiceDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DBServiceRepository(
    private val dataSource: DataSource,
    private val appExecutors: AppExecutors
) {

    val listServices:LiveData<List<Service>> = Transformations.map(dataSource.listServices()){
        it.map { itDBServiceDTO ->
            itDBServiceDTO.toService()
        }
    }

    suspend fun addService(service: Service) = withContext(Dispatchers.IO){
        dataSource.add(
            DBServiceDTO(
                service.name, service.description, service.timestamp
            )
        )
    }

    suspend fun updateService(service: Service) =  withContext(Dispatchers.IO){
        dataSource.update(
            service.id,
            DBServiceDTO(
                service.name, service.description, service.timestamp
            )
        )
    }

    suspend fun deleteService(service: Service) = withContext(Dispatchers.IO){
        try {
            dataSource.delete(
                service.id,
                DBServiceDTO(
                    service.name, service.description, service.timestamp
                )
            )
        } catch (e: Exception){}

    }

}