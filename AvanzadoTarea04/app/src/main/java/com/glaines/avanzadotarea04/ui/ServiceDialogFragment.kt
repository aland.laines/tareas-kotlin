package com.glaines.avanzadotarea04.ui

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import java.lang.ClassCastException

class ServiceDialogFragment : DialogFragment() {

    private var listener: DialogListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val title = arguments?.getString("TITLE")
        val type = arguments?.getInt("TYPE")

        return AlertDialog.Builder(requireContext())
            .setTitle(title)
            .setPositiveButton("ACEPTAR") { _, _ ->
                listener?.onPositiveListener(null, type ?: 0)
            }
            .setNegativeButton("CANCELAR") { _, _ ->
                listener?.onNegativeListener(null, type ?: 0)
            }
            .create()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        try {
            listener = targetFragment as? DialogListener
        } catch (e: ClassCastException){
            throw ClassCastException((context.toString() +
                    " must implement NoticeDialogListener"))
        }

    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface DialogListener {
        fun onPositiveListener(any: Any?, type: Int)
        fun onNegativeListener(any: Any?, type: Int)
    }
}