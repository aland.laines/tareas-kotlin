package com.glaines.avanzadotarea04.extensions

import android.content.Context
import android.widget.Toast
import androidx.fragment.app.Fragment

fun Context.nToast(value: String) {
    Toast.makeText(this, value, Toast.LENGTH_LONG).show()
}

fun Fragment.nToast(value: String) {
    Toast.makeText(requireContext(), value, Toast.LENGTH_LONG).show()
}