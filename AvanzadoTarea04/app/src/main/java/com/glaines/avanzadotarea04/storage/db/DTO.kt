package com.glaines.avanzadotarea04.storage.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.glaines.avanzadotarea04.model.Service
import java.util.Date

@Entity(tableName = "tb_services")
data class DBServiceDTO(
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "description") val description: String?,
    @ColumnInfo(name = "timestamp") val timestamp: Date?
) {

    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
    override fun toString(): String {
        return "DBService(id=$id, name=$name, description=$description) , timestamp=$timestamp)"
    }

    fun toService():Service = Service(id?:0,name?:"",description?:"",timestamp?:Date())
}