package com.glaines.avanzadotarea04.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.glaines.avanzadotarea04.di.Injector
import com.glaines.avanzadotarea04.R
import com.glaines.avanzadotarea04.databinding.FragmentMainBinding
import com.glaines.avanzadotarea04.extensions.nToast
import com.glaines.avanzadotarea04.model.Service

class MainFragment : Fragment() {

    private val repository by lazy {
        Injector.provideServiceRepository()
    }
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter: ServiceAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ui()
        repository.listServices.observe(viewLifecycleOwner, Observer {
            adapter.update(it)
        })
    }

    private fun ui() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        adapter =
            ServiceAdapter(emptyList()) { itService ->
                showMessage("item $itService")
                goToService(itService)
            }
        binding.recyclerView.adapter = adapter

        binding.floatingActionButton.setOnClickListener {
            goToAddService()
        }
    }

    private fun goToAddService() {
        findNavController().navigate(R.id.action_mainFragment_to_addServiceFragment)
    }

    private fun goToService(service: Service) {
        findNavController().navigate(R.id.action_mainFragment_to_editServiceFragment, Bundle().apply {
            putSerializable("SERVICE", service)
        })
    }

    private fun showMessage(item: String) {
        Log.v("CONSOLE", "item $item")
        nToast("item $item")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}