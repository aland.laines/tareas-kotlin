package com.glaines.avanzadotarea04.model

import androidx.annotation.Keep
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

@Keep
data class Service(val id: Int, val name: String, val description: String, val timestamp: Date) : Serializable{

    override fun toString(): String {
        return "Note(id=$id, name=$name, description=$description) , timestamp=$timestamp)"
    }

    fun time(): String = dateFormat.format(timestamp)

    private val dateFormat by lazy {
        SimpleDateFormat("dd/MM")
    }

}