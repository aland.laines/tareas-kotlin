package com.glaines.avanzadotarea04

import android.app.Application
import com.glaines.avanzadotarea04.di.Injector

class ServiceApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        com.glaines.avanzadotarea04.di.Injector.setup(this)
    }
}