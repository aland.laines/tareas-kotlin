package com.glaines.avanzadotarea04.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.glaines.avanzadotarea04.databinding.RowServiceUpBinding
import com.glaines.avanzadotarea04.model.Service

class ServiceAdapter(
    private var services: List<Service>,
    private val itemCallback: (service: Service) -> Unit?
) : RecyclerView.Adapter<ServiceAdapter.ServiceViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceViewHolder {
        val viewBinding =
            RowServiceUpBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ServiceViewHolder(viewBinding)
    }

    override fun getItemCount(): Int = services.size

    override fun onBindViewHolder(holder: ServiceViewHolder, position: Int) {
        holder.bind(services[position])
    }

    fun update(data: List<Service>) {
        services = data
        notifyDataSetChanged()
    }

    inner class ServiceViewHolder(private val viewBinding: RowServiceUpBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {
        fun bind(service: Service) {
            viewBinding.tviName.text = service.name
            viewBinding.tviDesc.text = service.description
            viewBinding.tviDate.text = service.time()

            viewBinding.root.setOnClickListener {
                itemCallback(service)
            }
        }
    }
}