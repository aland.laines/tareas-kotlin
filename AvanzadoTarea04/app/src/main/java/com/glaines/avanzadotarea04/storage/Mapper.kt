package com.glaines.avanzadotarea04.storage

import com.glaines.avanzadotarea04.model.Service
import com.glaines.avanzadotarea04.storage.db.DBServiceDTO
import java.util.Date

object Mapper {

    fun serviceDbToService(dbService:DBServiceDTO): Service {
        return Service(
            dbService.id ?: 0,
            dbService.name ?: "",
            dbService.description ?: "",
            dbService.timestamp ?: Date()
        )
    }

    fun serviceToDbService(service: Service): DBServiceDTO {
        return DBServiceDTO(service.name, service.description, service.timestamp)
    }

    fun mapList(serviceList: List<Service>): List<DBServiceDTO> {
        return serviceList.map { serviceToDbService(it) }
    }

    fun mapDbList(serviceDBList: List<DBServiceDTO>): List<Service> {
        return serviceDBList.map { serviceDbToService(it) }
    }

}