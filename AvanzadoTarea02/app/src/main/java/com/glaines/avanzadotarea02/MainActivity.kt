package com.glaines.avanzadotarea02

import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.glaines.avanzadotarea02.ui.FavoritesFragment
import com.glaines.avanzadotarea02.ui.NoticesFragment
import com.glaines.avanzadotarea02.ui.VideosFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), TabNavListener, View.OnClickListener {

    private val FRAGMENT_NOTICES = 0
    private val FRAGMENT_VIDEOS = 1
    private val FRAGMENT_FAVORITES = 2

    private val indicatorViews = mutableListOf<View>()
    private val titleViews = mutableListOf<TextView>()
    private var currentIndicator = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        indicatorViews.add(vNotices)
        indicatorViews.add(vVideos)
        indicatorViews.add(vFavorites)

        titleViews.add(tvNotices)
        titleViews.add(tvVideos)
        titleViews.add(tvFavorites)

        titleViews.forEach {
            it.setOnClickListener(this)
        }

        selectFirst()

    }

    override fun onClick(view: View?) {
        val bundle = Bundle()

        val fragmentId = when (view?.id) {
            R.id.tvNotices -> FRAGMENT_NOTICES
            R.id.tvVideos -> FRAGMENT_VIDEOS
            R.id.tvFavorites -> FRAGMENT_FAVORITES

            else -> FRAGMENT_NOTICES
        }

        updateUI(fragmentId)
        changeFragment(bundle, fragmentId)
    }

    private fun selectFirst() {
        val bundle = Bundle()
        val fragmentId = FRAGMENT_NOTICES
        updateUI(fragmentId)
        changeFragment(bundle, fragmentId)
    }

    private fun updateUI(fragmentId: Int) {
        if (currentIndicator >= 0) {
            indicatorViews[currentIndicator].setBackgroundColor(Color.TRANSPARENT)
            titleViews[currentIndicator].inputType = Typeface.NORMAL
        }
        indicatorViews[fragmentId].setBackgroundColor(Color.parseColor("#ffeb3b"))
        titleViews[fragmentId].inputType = Typeface.BOLD
        currentIndicator = fragmentId
    }

    private fun changeFragment(bundle: Bundle, fragmentId: Int) {

        val fragment = factoryFragment(bundle, fragmentId)

        fragment?.let {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.fontainer, it)
                addToBackStack(null)
                commit()
            }
        }
    }

    private fun factoryFragment(bundle: Bundle, fragmentId: Int): Fragment? {
        when (fragmentId) {
            FRAGMENT_NOTICES -> return NoticesFragment()
            FRAGMENT_VIDEOS -> return VideosFragment()
            FRAGMENT_FAVORITES -> return FavoritesFragment()
        }
        return null
    }
}