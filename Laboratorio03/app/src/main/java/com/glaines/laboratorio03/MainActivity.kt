package com.glaines.laboratorio03

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.sleep(2000)
        setTheme(R.style.Theme_Laboratorio03)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnRegistrar.setOnClickListener {
            val nombre = edtNombre.text.toString()
            val edad = edtEdad.text.toString()

            if (nombre.isEmpty()){
                this.mensaje("Debe ingresar nombre")
                return@setOnClickListener
            }
            if (edad.isEmpty()){
                this.mensaje("Debe ingresar edad")
                return@setOnClickListener
            }
            var tipoMascota : String
            if(perro.isChecked){
                tipoMascota = "Perro"
            } else if(gato.isChecked){
                tipoMascota = "Gato"
            } else if(conejo.isChecked){
                tipoMascota = "Conejo"
            } else  {
                this.mensaje("Debe elegir al menos un tipo")
                return@setOnClickListener
            }
            val vacunas = ArrayList<String>()
            if(cbDistemper.isChecked) vacunas.add("Distemper")
            if(cbHepatitis.isChecked) vacunas.add("Hepatitis")
            if(cbLeptospirosis.isChecked) vacunas.add("Leptospirosis")
            if(cbParvovirus.isChecked) vacunas.add("Parvovirus")
            if(cbRabia.isChecked) vacunas.add("Rabia")

            val bundle = Bundle()
            bundle.apply {
                putString("key_nombre",nombre)
                putString("key_edad",edad)
                putString("key_tmascota",tipoMascota)
                putSerializable("key_vacunas", vacunas)
            }

            val intent = Intent(this,ResultadoActivity::class.java).apply {
                putExtras(bundle)
            }

            startActivity(intent)

        }

    }
    private fun Context.mensaje(mensaje:String) {
        Toast.makeText(this,"$mensaje",Toast.LENGTH_SHORT).show()
        Toast.makeText(this,mensaje,Toast.LENGTH_SHORT).show()
    }
}