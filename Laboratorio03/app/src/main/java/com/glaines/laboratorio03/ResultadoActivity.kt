package com.glaines.laboratorio03

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_resultado.*

class ResultadoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resultado)
        val bundle : Bundle? = intent.extras

        bundle.let {
            val nombre =  it?.getString("key_nombre")
            val edad = it?.getString("key_edad")
            val tipo = it?.getString("key_tmascota")
            val vacunas = it?.getSerializable("key_vacunas")

            tvNombre.text = "Nombres: $nombre"
            tvEdad.text = "Edad: $edad"
            when(tipo){
                "Perro" -> icoMascota.setImageResource(R.drawable.perro)
                "Gato" -> icoMascota.setImageResource(R.drawable.gato)
                "Conejo" -> icoMascota.setImageResource(R.drawable.conejo)
            }
            tvVacunas.text = vacunas.toString()

        }
    }
}