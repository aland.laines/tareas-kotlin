package com.glaines.taxgenapp

import android.content.Context
import android.os.Bundle
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main2.*

@Suppress("SpellCheckingInspection")
class MainActivity : AppCompatActivity() {

    private val z = arrayListOf("Generación Z","1994 - 2010","7.800.000","Expansion masiva de internet","Irreverencia")
    private val y = arrayListOf("Generación Y","1981 - 1993","7.200.000","Inicio de la Digitalizacion","Frustración")
    private val x = arrayListOf("Generación X","1969 - 1980","9.300.000","Crisis del 73 y transición española","Obsesión por el éxito")
    private val bB = arrayListOf("Baby Boom","1949 - 1968","12.200.000","Paz y explosión demográfica", "Ambición")
    private val sG = arrayListOf("Silent Generation","1930 - 1948","6.300.000","Conflictos bélicos","Austeridad")

    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.sleep(2000)
        setTheme(R.style.Theme_TaxGenApp)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        btnVerificar.setOnClickListener {
            hideKeyboard()
            val yearBirth = edtNacimiento.text.toString()
            when(yearBirth.toInt()){
                in 1930..1948 -> {
                    tvGeneracion.text = sG[0]
                    tvYears.text = sG[1]
                    tvPoblacion.text = getString(R.string.poblacion, sG[2])
                    tvDescrip.text = sG[3]
                    tvRasgo.text = getString(R.string.rasgo, sG[4])
                }
                in 1949..1968 -> {
                    tvGeneracion.text = bB[0]
                    tvYears.text = bB[1]
                    tvPoblacion.text = getString(R.string.poblacion, bB[2])
                    tvDescrip.text = bB[3]
                    tvRasgo.text = getString(R.string.rasgo, bB[4])
                }
                in 1969..1980 -> {
                    tvGeneracion.text = x[0]
                    tvYears.text = x[1]
                    tvPoblacion.text = getString(R.string.poblacion, x[2])
                    tvDescrip.text = x[3]
                    tvRasgo.text = getString(R.string.poblacion, x[4])
                }
                in 1981..1993 -> {
                    tvGeneracion.text = y[0]
                    tvYears.text = y[1]
                    tvPoblacion.text = getString(R.string.poblacion, y[2])
                    tvDescrip.text = y[3]
                    tvRasgo.text = getString(R.string.poblacion, y[4])
                }
                in 1994..2010 -> {
                    tvGeneracion.text = z[0]
                    tvYears.text = z[1]
                    tvPoblacion.text = getString(R.string.poblacion, z[2])
                    tvDescrip.text = z[3]
                    tvRasgo.text = getString(R.string.poblacion, z[4])
                }
                else -> {
                    tvGeneracion.text = getString(R.string.noDefinida)
                    tvYears.text = ""
                    tvPoblacion.text = ""
                    tvDescrip.text = ""
                    tvRasgo.text = ""
                }
            }
        }

    }

    private fun AppCompatActivity.hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

}