package com.glaines.avanzadotarea03

import android.app.Application
import com.glaines.avanzadotarea03.di.Injector

class ServiceApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        Injector.setup(this)
    }
}