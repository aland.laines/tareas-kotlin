package com.glaines.avanzadotarea03.storage

import com.glaines.avanzadotarea03.storage.db.DBServiceDTO

interface DataSource {

    fun listServices(): List<DBServiceDTO>
    fun add(service: DBServiceDTO)
    fun update(id: Int, service: DBServiceDTO)
    fun delete(id: Int, service: DBServiceDTO)

}