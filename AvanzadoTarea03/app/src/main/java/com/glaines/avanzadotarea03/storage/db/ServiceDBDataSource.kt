package com.glaines.avanzadotarea03.storage.db

import com.glaines.avanzadotarea03.storage.DataSource

class ServiceDBDataSource(private val dbService: DBService): DataSource {

    private val dao = dbService.serviceDao()

    override fun listServices(): List<DBServiceDTO> = dao.services()

    override fun add(service: DBServiceDTO) {
        dao.insertService(service)
    }

    override fun update(id: Int, service: DBServiceDTO) {
        service.id = id
        dao.updateService(service)
    }

    override fun delete(id: Int, service: DBServiceDTO) {
        service.id = id
        dao.deleteService(service)
    }
}