package com.glaines.avanzadotarea03.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.glaines.avanzadotarea03.R
import com.glaines.avanzadotarea03.databinding.FragmentEditServiceBinding
import com.glaines.avanzadotarea03.di.Injector
import com.glaines.avanzadotarea03.model.Service
import java.util.*

class EditServiceFragment: Fragment(), ServiceDialogFragment.DialogListener {

    private val repository by lazy {
        Injector.provideServiceRepository()
    }

    private var _binding: FragmentEditServiceBinding? = null
    private val binding get() = _binding!!

    private var service: Service? = null
    private var name: String = ""
    private var desc: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        service = arguments?.getSerializable("SERVICE") as? Service
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEditServiceBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populate()
        binding.btnEditService.setOnClickListener {
            if(validForm()){
                editService()
                findNavController().popBackStack()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.edit_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_delete -> {
                showServiceDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showServiceDialog() {
        val serviceDialogFragment = ServiceDialogFragment()
        val bundle = Bundle().apply {
            putString("TITLE","¿Quieres elminiar este servicio?")
            putInt("TYPE", 100)
        }
        serviceDialogFragment.arguments = bundle
        serviceDialogFragment.setTargetFragment(this, 0)
        serviceDialogFragment.show(requireFragmentManager(), "ServiceDialogFragment")
    }

    private fun editService() {
        val serviceId: Int = service?.id ?: 0
        val nService = Service(serviceId, name, desc, service?.timestamp ?: Date())

        repository.updateService(nService) {}
    }

    private fun validForm(): Boolean {
        name = binding.edtName.text.toString()
        desc = binding.edtDesc.text.toString()

        if(name.isEmpty()){
            return false
        }

        if (desc.isEmpty()){
            return false
        }
        return true
    }

    private fun populate() {
        service?.let {
            binding.edtName.setText(it.name)
            binding.edtDesc.setText(it.description)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onPositiveListener(any: Any?, type: Int) {
        service?.let {
            repository.deleteService(it) {
            }
        }
        findNavController().popBackStack()
    }

    override fun onNegativeListener(any: Any?, type: Int) {}

}