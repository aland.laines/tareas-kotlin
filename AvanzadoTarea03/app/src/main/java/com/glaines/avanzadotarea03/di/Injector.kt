package com.glaines.avanzadotarea03.di

import android.content.Context
import com.glaines.avanzadotarea03.AppExecutors
import com.glaines.avanzadotarea03.storage.DBServiceRepository
import com.glaines.avanzadotarea03.storage.DataSource
import com.glaines.avanzadotarea03.storage.db.DBService
import com.glaines.avanzadotarea03.storage.db.ServiceDBDataSource

object Injector {

    private val appExecutors: AppExecutors = AppExecutors()
    private lateinit var dataSource: DataSource
    private lateinit var serviceRepository: DBServiceRepository

    fun setup(context: Context) {
        DBService.getInstance(context)?.let {
            dataSource = ServiceDBDataSource(it)
            it
        }
        serviceRepository = DBServiceRepository(dataSource, appExecutors)
    }

    private fun provideAppExecutors() = appExecutors
    private fun provideDataSource() = dataSource
    fun provideServiceRepository() = serviceRepository

    fun destroy() {
        DBService.destroyInstance()
    }
}