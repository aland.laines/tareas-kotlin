package com.glaines.avanzadotarea03.storage.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import java.lang.Exception

@Database(entities = [DBServiceDTO::class], version = 1)
@TypeConverters(Converters::class)
abstract class DBService : RoomDatabase() {

    abstract fun serviceDao(): DBServiceDao

    companion object {
        private const val DB_NAME = "DB_SERVICE.db"
        private var INSTANCE: DBService? = null

        fun getInstance(context: Context): DBService? {
            if (INSTANCE == null) {
                synchronized(DBService::class.java) {
                    INSTANCE = Room.databaseBuilder(context, DBService::class.java, DB_NAME).build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

    }
}