package com.glaines.avanzadotarea03.storage.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface DBServiceDao {

    @Query("SELECT * FROM tb_services")
    fun services():List<DBServiceDTO>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertService(service: DBServiceDTO)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateService(service: DBServiceDTO)

    @Delete
    fun deleteService(service: DBServiceDTO)
}