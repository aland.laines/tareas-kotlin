package com.glaines.avanzadotarea03.ui.util

import androidx.recyclerview.widget.DiffUtil
import com.glaines.avanzadotarea03.model.Service

class ServiceDiffUtilCallback(private val oldList: List<Service>, private val newList: List<Service>) :
    DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}