package com.glaines.avanzadotarea03.storage

import com.glaines.avanzadotarea03.AppExecutors
import com.glaines.avanzadotarea03.model.Service

class DBServiceRepository(
    private val dataSource: DataSource,
    private val appExecutors: AppExecutors
) {

    fun listService(callback: (services: List<Service>) -> Unit) {
        appExecutors.diskIO.execute {
            val serviceList = Mapper.mapDbList(dataSource.listServices())
            appExecutors.mainThread.execute { callback(serviceList) }
        }
    }

    fun addService(service: Service, callback: () -> Unit) {
        appExecutors.diskIO.execute {
            dataSource.add(Mapper.serviceToDbService(service))
            appExecutors.mainThread.execute { callback() }
        }
    }

    fun updateService(service: Service, callback: () -> Unit) {
        appExecutors.diskIO.execute{
            dataSource.update(service.id, Mapper.serviceToDbService(service))
            appExecutors.mainThread.execute { callback() }
        }
    }

    fun deleteService(service: Service, callback: () -> Unit) {
        appExecutors.diskIO.execute{
            dataSource.delete(service.id, Mapper.serviceToDbService(service))
            appExecutors.mainThread.execute { callback() }
        }
    }

}