package com.glaines.avanzadotarea03.model

import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

data class Service(val id: Int, val name: String, val description: String, val timestamp: Date) : Serializable{
    override fun toString(): String {
        return "Note(id=$id, name=$name, description=$description) , timestamp=$timestamp)"
    }

    private val dateFormat by lazy {
        SimpleDateFormat("dd/MM")
    }

    fun time(): String = dateFormat.format(timestamp)
}