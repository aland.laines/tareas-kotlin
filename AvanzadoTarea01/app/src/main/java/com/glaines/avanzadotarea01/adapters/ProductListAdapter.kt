package com.glaines.avanzadotarea01.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.glaines.avanzadotarea01.ProductViewModel
import com.glaines.avanzadotarea01.R
import com.glaines.avanzadotarea01.databinding.CardProductItemBinding
import com.glaines.avanzadotarea01.entities.Product
import com.squareup.picasso.Picasso

class ProductListAdapter(var listProduct: List<Product> = listOf(), var callbackProduct: (Product) -> Unit) :
    RecyclerView.Adapter<ProductListAdapter.ProductListAdapterViewHolder>() {

    inner class ProductListAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        private val binding : CardProductItemBinding = CardProductItemBinding.bind(itemView)

        fun bind(producto : Product) = with(binding){
            tvProductName.text = producto.productName
            tvProductPrice.text = producto.productPrice.toString()
            Picasso.get().load(producto.productImage).error(R.drawable.no_image).into(imageProductItem)

            binding.root.setOnClickListener {
                callbackProduct(producto)
            }
        }
    }

    fun updateList(listProduct: List<Product>){
        this.listProduct = listProduct
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListAdapterViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.card_product_item, parent, false)
        return ProductListAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductListAdapterViewHolder, position: Int) {
        val producto:Product = listProduct[position]
        holder.bind(producto)
    }

    override fun getItemCount(): Int = listProduct.size

}