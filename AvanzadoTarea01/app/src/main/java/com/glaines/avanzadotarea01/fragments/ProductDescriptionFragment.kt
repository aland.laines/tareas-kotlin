package com.glaines.avanzadotarea01.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.glaines.avanzadotarea01.ProductViewModel
import com.glaines.avanzadotarea01.R
import com.glaines.avanzadotarea01.databinding.FragmentProductDescriptionBinding
import com.squareup.picasso.Picasso

class ProductDescriptionFragment : Fragment(R.layout.fragment_product_description) {

    private lateinit var binding : FragmentProductDescriptionBinding
    private val viewmodel : ProductViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentProductDescriptionBinding.bind(view)

        viewmodel._datos.observe(viewLifecycleOwner, Observer {
            binding.tvProductDetailName.text = it.productName
            binding.tvProductDetailPrice.text = it.productPrice.toString()
            binding.tvProductDetailDescription.text = it.productDescription
            Picasso.get().load(it.productImage).error(R.drawable.no_image).into(binding.imageProduct)
        })
    }

}