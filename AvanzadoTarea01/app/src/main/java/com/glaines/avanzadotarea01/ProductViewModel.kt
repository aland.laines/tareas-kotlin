package com.glaines.avanzadotarea01

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.glaines.avanzadotarea01.entities.Product

class ProductViewModel : ViewModel() {

    var _datos : MutableLiveData<Product> = MutableLiveData()

    fun datos() : LiveData<Product>{
        return _datos
    }

    fun setData(dataProduct:Product){
        _datos.value = dataProduct
    }

}