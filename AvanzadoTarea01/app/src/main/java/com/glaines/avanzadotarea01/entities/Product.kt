package com.glaines.avanzadotarea01.entities

import java.io.Serializable

data class Product(
    var id: Int,
    var productName: String,
    var productDescription: String,
    var productPrice: Double,
    var productImage: Int) : Serializable
