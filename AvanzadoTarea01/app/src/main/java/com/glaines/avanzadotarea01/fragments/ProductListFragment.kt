package com.glaines.avanzadotarea01.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.glaines.avanzadotarea01.ProductViewModel
import com.glaines.avanzadotarea01.R
import com.glaines.avanzadotarea01.adapters.ProductListAdapter
import com.glaines.avanzadotarea01.databinding.FragmentProductListBinding
import com.glaines.avanzadotarea01.entities.Product

class ProductListFragment : Fragment(R.layout.fragment_product_list) {

    private lateinit var binding : FragmentProductListBinding
    private lateinit var adaptador : ProductListAdapter
    private val viewmodel : ProductViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentProductListBinding.bind(view)

        configurarAdapter()
        cargarDatosProductos()

    }

    private fun cargarDatosProductos() {
        val productos = listOf(
            Product(1,"Corte Clásico","descripción de un clasico",15.0,R.drawable.p01),
            Product(2,"Corte Diseño","descripción de un diseño",45.0,R.drawable.p02),
            Product(3,"Servicio Barber","descripción de un barber",60.0,R.drawable.p03),
            Product(4,"Servicio VIP","descripción de un VIP",90.0,R.drawable.p04),
        )
        adaptador.updateList(productos)
    }

    private fun configurarAdapter() {
        adaptador = ProductListAdapter(){
            viewmodel.setData(it)
        }
        binding.rvProducts.adapter = adaptador
        binding.rvProducts.layoutManager = LinearLayoutManager(this.context)
    }
}
