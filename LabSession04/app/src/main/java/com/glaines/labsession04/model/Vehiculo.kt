package com.glaines.labsession04.model

data class Vehiculo( val titular:String, val marca:String, val anio:Int, val problema:String)
