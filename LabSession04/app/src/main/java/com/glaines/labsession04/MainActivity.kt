package com.glaines.labsession04

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.glaines.labsession04.databinding.ActivityMainBinding
import com.glaines.labsession04.model.Vehiculo

class MainActivity : AppCompatActivity() {

    private lateinit var binding:ActivityMainBinding
    var marca = ""
    val marcas = listOf<String>(
        "Seleccione Marca de Auto",
        "Alfa Romeo",
        "Aston Martin",
        "Audi",
        "Bentley",
        "BMW",
        "Cadillac",
        "Chevrolet",
        "Citroen",
        "Ferrari",
        "Fiat",
        "Ford",
        "Honda",
        "Isuzu",
        "Jaguar",
        "Jeep",
        "Kia",
        "KTM",
        "Lada",
        "Lamborghini",
        "Land Rover",
        "Lexus",
        "Lotus",
        "Maserati",
        "Mazda",
        "Mercedes-Benz",
        "Mini",
        "Mitsubishi",
        "Nissan",
        "Opel",
        "Peugeot",
        "Piaggio",
        "Porsche",
        "Renault",
        "Rolls-Royce",
        "SsangYong",
        "Subaru",
        "Suzuki",
        "Tesla",
        "Toyota",
        "Volkswagen",
        "Volvo"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.sleep(2000)
        setTheme(R.style.Theme_LabSession04)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val adaptador = ArrayAdapter(this, R.layout.spinner_style_layout, marcas)
        binding.spMarcas.adapter = adaptador

        binding.spMarcas.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

            override fun onItemSelected( parent: AdapterView<*>?, view: View?, position: Int, id: Long ) {
                marca = marcas[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }

        binding.btnRegistrar.setOnClickListener {
            if(marca == "Seleccione Marca de Auto"){
                Toast.makeText(this,"Debe seleccionar una Marca de Auto", Toast.LENGTH_SHORT).show()
            } else {
                val propietario = binding.edtNombres.text.toString()
                val anio = binding.edtAnio.text.toString().toInt()
                val problema = binding.edtProblema.text.toString()
                val vehiculo = Vehiculo(propietario, marca, anio, problema)
                mostrarDialogo(vehiculo).show()
            }
        }
    }

    private fun mostrarDialogo(vehiculo: Vehiculo): AlertDialog {
        val alertDialog  :AlertDialog
        val builder = AlertDialog.Builder(this)
        val view:View = layoutInflater.inflate(R.layout.dialogo_vehiculo, null)
        builder.setView(view)

        val btnAceptar : Button = view.findViewById(R.id.btnConfirmar)
        val btnCancelar : Button = view.findViewById(R.id.btnCancelar)

        val tvPropietario : TextView = view.findViewById(R.id.tvPropietario)
        val tvMarca : TextView = view.findViewById(R.id.tvMarca)
        val tvAnio : TextView = view.findViewById(R.id.tvAnio)
        val tvProblema : TextView = view.findViewById(R.id.tvProblema)

        tvPropietario.text = vehiculo.titular
        tvMarca.text = vehiculo.marca
        tvAnio.text = vehiculo.anio.toString()
        tvProblema.text = vehiculo.problema

        alertDialog = builder.create()
        alertDialog.setCancelable(false)

        btnAceptar.setOnClickListener { alertDialog.dismiss() }
        btnCancelar.setOnClickListener { alertDialog.dismiss() }

        return alertDialog
    }

}